<?php
declare(strict_types=1);

namespace OOP;

include "../vendor/autoload.php";

use PHPUnit\Framework\TestCase;


/**
 * Class SquareTest
 * @package OOP
 */
class SquareTest extends TestCase
{

    public function testGetSide()
    {
        $square = new Square(10);
        static::assertEquals(10, $square->getSide());
    }
}
