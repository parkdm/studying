<?php
declare(strict_types=1);

include "../vendor/autoload.php";

use OOP\Random;
use PHPUnit\Framework\TestCase;

/**
 * Class RandomTest
 */
class RandomTest extends TestCase {
    public function testGetNext() {
        $random = new Random(100);
        static::assertEquals(true, $random->getNext() != 100);
    }

    public function testConstruct() {
        $random = new Random(100);
        static::assertEquals(true, $random->value == 100);
    }

    public function testReset() {
        $random = new Random(100);
        $random->getNext();
        $random->reset();
        static::assertEquals(100, $random->value);
    }
}
