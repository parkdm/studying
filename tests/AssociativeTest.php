<?php
declare(strict_types=1);

include "../vendor/autoload.php";

use PHPUnit\Framework\TestCase;
use function App\AssocArrays\buildQueryString;
use function App\AssocArrays\findWhere;
use function App\AssocArrays\fromParis;
use function App\AssocArrays\toRna;


/**
 * Class AssociativeTest
 */
class AssociativeTest extends TestCase
{
    /** @test **/
    public function fromParisTest1(){
        static::assertEquals(
            [],
            fromParis([])
        );
    }

    /** @test **/
    public function fromParisTest2(){
        static::assertEquals(
            [],
            fromParis([])
        );
    }

    /** @test **/
    public function  buildQueryStringTest1(){
        static::assertEquals(
            'page=1&per=10',
            buildQueryString(['per' => 10, 'page' => 1])
        );
    }

    /** @test **/
    public function  buildQueryStringTest2(){
        static::assertEquals(
            '',
            buildQueryString([])
        );
    }


    /** @test **/
    public function  findWhereTest1(){
        $books = [
            ['title' => 'Book of Fooos', 'author' => 'FooBar', 'year' => 1111],
            ['title' => 'Cymbeline', 'author' => 'Shakespeare', 'year' => 1611],
            ['title' => 'The Tempest', 'author' => 'Shakespeare', 'year' => 1611],
            ['title' => 'Book of Foos Barrrs', 'author' => 'FooBar', 'year' => 2222],
            ['title' => 'Still foooing', 'author' => 'FooBar', 'year' => 3333],
            ['title' => 'Happy Foo', 'author' => 'FooBar', 'year' => 4444],
        ];
        $params = [
            'author' => 'Shakespeare',
            'year' => 1611
        ];
        static::assertEquals(
            ['title' => 'Cymbeline', 'author' => 'Shakespeare', 'year' => 1611],
            findWhere($books, $params)
        );
    }

    /** @test **/
    public function  findWhereTest(){
        $books = [
            ['title' => 'Book of Fooos', 'author' => 'FooBar', 'year' => 1111],
            ['title' => 'Cymbeline', 'author' => 'Shakespeare', 'year' => 1611],
            ['title' => 'The Tempest', 'author' => 'Shakespeare', 'year' => 1611],
            ['title' => 'Book of Foos Barrrs', 'author' => 'FooBar', 'year' => 2222],
            ['title' => 'Still foooing', 'author' => 'FooBar', 'year' => 3333],
            ['title' => 'Happy Foo', 'author' => 'FooBar', 'year' => 4444],
        ];
        $params = [
            'author' => 'Shakespeare',
            'year' => 1611
        ];

        static::assertEquals(
            ['title' => 'Cymbeline', 'author' => 'Shakespeare', 'year' => 1611],
            findWhere($books, $params)
        );

        $params['year'] = 1612;
        static::assertEquals(
            null,
            findWhere($books, $params)
        );
    }

    /** @test **/
    public function  toRnaTest1(){
        static::assertEquals(
            'UGCACCAGAAUU',
            toRna('ACGTGGTCTTAA')
        );
    }

    /** @test **/
    public function  toRnaTest2(){
        static::assertEquals(
            null,
            toRna('щCG5TGGTfCTTA')
        );
    }

    /** @test **/
    public function  toRnaTest3(){
        static::assertEquals(
            null,
            toRna('')
        );
    }
}
