<?php
declare(strict_types=1);

namespace OOP;

include '../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

/**
 * Class UrlTest
 * @package OOP
 */
class UrlTest extends TestCase
{
    public function testGetScheme()
    {
        $url = new Url('http://yandex.ru?key=value&key2=value2');
        static::assertEquals('http', $url->getScheme());
    }

    public function testGetQueryParam1()
    {
        $url = new Url('http://yandex.ru?key=value&key2=value2');
        static::assertEquals('value', $url->getQueryParam('key'));
    }

    public function testGetQueryParam2()
    {
        $url = new Url('http://yandex.ru?key=value&key2=value2');
        static::assertEquals('value2', $url->getQueryParam('key2', 'lala'));
    }

    public function testGetQueryParam3()
    {
        $url = new Url('http://yandex.ru?key=value&key2=value2');
        static::assertEquals('ehu', $url->getQueryParam('new', 'ehu'));
    }

    public function testGetQueryParams()
    {
        $url = new Url('http://yandex.ru?key=value&key2=value2');
        static::assertEquals(
            [
                'key' => 'value',
                'key2' => 'value2'
            ],
            $url->getQueryParams());
    }

    public function testGetHost()
    {
        $url = new Url('http://yandex.ru?key=value&key2=value2');
        static::assertEquals('yandex.ru', $url->getHost());
    }
}
