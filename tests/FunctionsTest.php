<?php
declare(strict_types=1);

include "../vendor/autoload.php";

use PHPUnit\Framework\TestCase;
use function App\Functions\decode;
use function App\Functions\enlargeArrayImage;
use function App\Functions\findIndexOfNearest;
use function App\Functions\getSameParity;

/**
 * Class FunctionsTest
 */
class FunctionsTest extends TestCase
{
    /** @test **/
    function findIndexOfNearestTest1(){
        static::assertEquals(
            null,
            findIndexOfNearest([],2));
    }

    /** @test **/
    function findIndexOfNearestTest2(){
        static::assertEquals(
            2,
            findIndexOfNearest([15, 10, 3, 4], 0));
    }


    /** @test **/
    function getSameParityTest1(){
        static::assertEquals(
            [],
            getSameParity([]));
    }

    /** @test **/
    function getSameParityTest2(){
        static::assertEquals(
            [-1, 1, -3],
            getSameParity([-1, 0, 1, -3, 10, -2]));
    }

    /** @test **/
    function getSameParityTest3(){
        static::assertEquals(
            [2, 0, 10, -2],
            getSameParity([2, 0, 1, -3, 10, -2]));
    }


    /** @test **/
    function decodeTest1(){
        static::assertEquals(
            '011000110100',
            decode('_|¯|____|¯|__|¯¯¯'));
    }

    /** @test **/
    function decodeTest2(){
        static::assertEquals(
            '110010000100111',
            decode('|¯|___|¯¯¯¯¯|___|¯|_|¯'));
    }

    /** @test **/
    function decodeTest3(){
        static::assertEquals(
            '010010000100111',
            decode('¯|___|¯¯¯¯¯|___|¯|_|¯'));
    }

    /** @test **/
    function decodeTest4(){
        static::assertEquals(
            '',
            decode(''));
    }


    /** @test **/
    function enlargeArrayImageTest(){
        $expected = [
            ['*','*','*','*','*','*','*','*'],
            ['*','*','*','*','*','*','*','*'],
            ['*','*',' ',' ',' ',' ','*','*'],
            ['*','*',' ',' ',' ',' ','*','*'],
            ['*','*',' ',' ',' ',' ','*','*'],
            ['*','*',' ',' ',' ',' ','*','*'],
            ['*','*','*','*','*','*','*','*'],
            ['*','*','*','*','*','*','*','*'],
        ];

        $input = [
            ['*','*','*','*'],
            ['*',' ',' ','*'],
            ['*',' ',' ','*'],
            ['*','*','*','*']
        ];

        static::assertEquals(
            $expected,
            enlargeArrayImage($input));
    }
}
