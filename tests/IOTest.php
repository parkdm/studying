<?php
declare(strict_types=1);

include "../vendor/autoload.php";

use PHPUnit\Framework\TestCase;

/**
 * Class IOTest
 */
class IOTest extends TestCase
{
    /** @test **/
    public function scanDirectoryTest(){
        $expected = 'Файл Thumbs.db в папке Desktop 536.576Кб 2019.07.26
Файл Screenshot 2019-03-30 at 19.57.40.png в папке Desktop 769.099Кб 2019.07.25
Файл kinopoisk.ru-Die-Mitte-der-Welt-3039003.jpg в папке Desktop 351.156Кб 2019.07.25
Файл U4yxl0LeRC8.jpg в папке Desktop 50.799Кб 2019.07.25
Файл Screenshot 2019-06-25 at 10.59.26.png в папке Desktop 2676.552Кб 2019.07.29
Файл IMG_1447.jpg в папке Desktop 375.669Кб 2019.07.25
Файл Enigma-logo.jpg в папке Desktop 24.415Кб 2019.07.25
Файл CvKIz4wOZLY.jpg в папке Desktop 478.472Кб 2019.07.25
Файл Монтажная область 1.png в папке Desktop 24.856Кб 2019.07.25
Файл pubring.txk в папке Desktop 0.333Кб 2019.07.25
Файл Ij4c9lJcGVs.jpg в папке Desktop 136.384Кб 2019.07.25
Файл Screenshot 2019-05-14 at 01.54.29.png в папке Desktop 3836.283Кб 2019.07.29
Файл Screenshot 2019-01-23 at 23.35.43.png в папке Desktop 2624.165Кб 2019.07.25
Файл DU-G4uEW4AAbBUc.jpg в папке Desktop 506.53Кб 2019.07.25
Файл jf-process-14.png в папке Desktop 87.363Кб 2019.07.25
Файл 15402734829472.png в папке Desktop 33.49Кб 2019.07.25
Файл Screenshot 2019-01-22 at 17.05.18.png в папке Desktop 1600.792Кб 2019.07.25
Файл Screenshot 2019-05-10 at 15.31.25.png в папке Desktop 42.393Кб 2019.07.25
Файл .DS_Store в папке Desktop 6.148Кб 2019.07.28
Файл Screenshot 2019-06-28 at 12.33.59.png в папке Desktop 4901.573Кб 2019.07.29
Файл 617351368355646507972-1.jpg в папке Desktop 92.134Кб 2019.07.25
Файл reflectius-main.jpg в папке Desktop 39.973Кб 2019.07.25
Файл Screenshot 2019-01-20 at 23.46.16.png в папке Desktop 178.551Кб 2019.07.29
Файл p1SyGVeTWZo.jpg в папке Desktop 58.866Кб 2019.07.25
Файл Screenshot 2018-10-03 at 13.45.29.png в папке Desktop 28.492Кб 2019.07.29
Файл Screenshot 2019-05-02 at 17.17.36.png в папке Desktop 53.251Кб 2019.07.29
Файл .localized в папке Desktop 0Кб 2019.07.25
Файл Xiaomi_657B_wifi_conect.png в папке Desktop 4.316Кб 2019.07.25
Файл bilets.png в папке Desktop 143.778Кб 2019.07.25
Файл Screenshot 2019-07-27 at 01.37.42.png в папке Desktop 1279.483Кб 2019.07.29
Файл 1548583166196742888.jpg в папке Desktop 87.731Кб 2019.07.25
Файл Screenshot 2019-05-09 at 23.43.31.png в папке Desktop 407.442Кб 2019.07.25
Файл hX__J24_6Qo.jpg в папке Desktop 289.013Кб 2019.07.25
Файл maxresdefault.jpg в папке Desktop 38.057Кб 2019.07.25
Файл Screenshot 2019-01-25 at 12.06.33.png в папке Desktop 930.113Кб 2019.07.25
Файл Screenshot 2019-03-12 at 19.14.32.png в папке Desktop 67.54Кб 2019.07.29
Файл 1554318060132253132.jpg в папке Desktop 265.714Кб 2019.07.25
Файл bilets.ai в папке Desktop 520.273Кб 2019.07.26
Файл regulus_black_by_upthehilll-da2m8dv.jpg в папке Desktop 69.661Кб 2019.07.30
Файл IAD-RGZ-Ded.~ded в папке Desktop 180.482Кб 2019.07.25
Файл Screenshot 2019-01-22 at 21.01.07.png в папке Desktop 5452.003Кб 2019.07.29
Файл Screenshot 2019-05-10 at 02.18.23.png в папке Desktop 1198.381Кб 2019.07.25
Файл Screenshot 2019-02-10 at 13.23.39.png в папке Desktop 1151.523Кб 2019.07.29
Файл 2412763.jpg в папке Desktop 1689.484Кб 2019.07.25
Файл 915136636399138964296-1.gif в папке Desktop 20.103Кб 2019.07.25
Файл J7VJqN472os.jpg в папке Desktop 82.64Кб 2019.07.25
Файл Screen Shot 2018-09-03 at 19.09.53 копия.png в папке Desktop 1080.638Кб 2019.07.25
Файл 1520498759180325902.jpg в папке Desktop 31.363Кб 2019.07.25
Файл Screenshot 2019-07-21 at 20.48.45.png в папке Desktop 1997.061Кб 2019.07.29
Файл Screenshot 2018-12-08 at 18.05.26.png в папке Desktop 17.995Кб 2019.07.29
Файл Screenshot 2019-07-26 at 12.54.21.png в папке Desktop 7867.04Кб 2019.07.29
Файл IMAGE 2018-12-13 22:30:42.jpg в папке Desktop 46.074Кб 2019.07.25
Файл vlcsnap-2018-11-27-17h33m12s383.png в папке Desktop 1402.071Кб 2019.07.25
Файл Arnold_Boecklin_-_Island_of_the_Dead,_Third_Version.jpg в папке Desktop 757.614Кб 2019.07.25
Файл add_spaceInPrice.js в папке Desktop 0.251Кб 2019.07.29
Файл Screenshot 2019-04-30 at 18.50.29.png в папке Desktop 2113.888Кб 2019.07.29
Файл Screenshot 2019-04-15 at 17.45.10.png в папке Desktop 908.03Кб 2019.07.25
Файл 1549896019158881045.png в папке Desktop 1080.868Кб 2019.07.25
Файл Screenshot 2019-02-23 at 15.46.26.png в папке Desktop 21.1Кб 2019.07.29
Файл bilets-01.png в папке Desktop 1886.553Кб 2019.07.30
Файл sSXcDEkBhUk.jpg в папке Desktop 915.527Кб 2019.07.25
Файл Screenshot 2019-04-08 at 19.27.24.png в папке Desktop 66.955Кб 2019.07.25
Файл vlcsnap-2018-11-27-23h11m14s871.png в папке Desktop 1453.915Кб 2019.07.25
Файл kinopoisk.ru-Gwendoline-Christie-2560101.jpg в папке Desktop 420.879Кб 2019.07.25
Файл Axiom.sketch в папке Desktop 493.592Кб 2019.07.25
Файл 1532606313116847576.jpg в папке Desktop 149.311Кб 2019.07.25
Файл Nokia_wordmark.svg.png в папке Desktop 45.715Кб 2019.07.25
Файл photo_2019-07-26 23.16.29.jpeg в папке Desktop 40.293Кб 2019.07.30
Файл photo_2019-01-22 22.34.31.jpeg в папке Desktop 26.949Кб 2019.07.25
Файл Screenshot 2019-08-02 at 19.37.24.png в папке Desktop 124.008Кб 2019.08.02
Файл Screenshot 2019-01-05 at 20.51.34.png в папке Desktop 16.581Кб 2019.07.29
Файл 0211693a7a97034937d1e171533b055f.jpg в папке Desktop 174.178Кб 2019.07.25
Папка entodima - 2019.08.02
	Файл .DS_Store в папке entodima 6.148Кб 2019.07.28
	Папка entodima обложка.pxd - 2019.08.02
		Папка QuickLook - 2019.08.02
			Файл Preview.tiff в папке QuickLook 1827.314Кб 2019.07.25
			Файл Icon.tiff в папке QuickLook 4.322Кб 2019.07.25
			Файл Thumbnail.tiff в папке QuickLook 62.124Кб 2019.07.25
		Файл metadata.info в папке entodima обложка.pxd 126.976Кб 2019.07.25
		Папка data - 2019.08.02
			Файл D194C80D-0D01-4ECD-8CA7-AF8D49DC0E20 в папке data 41.685Кб 2019.07.25
			Файл 6811BD4F-EF79-4F6C-B4EA-E881CADAFFFF в папке data 3397.652Кб 2019.07.25
			Файл D284B8E8-3346-4444-8D9D-E1981FC3AFFD в папке data 1754.873Кб 2019.07.25
			Файл 35F27F19-61DD-44A8-B877-80570604148B в папке data 164.043Кб 2019.07.25
			Файл 977B63DD-0B50-4D4C-82A9-83B3B37B4967 в папке data 2937.858Кб 2019.07.25
			Файл FB8B652F-1544-42AC-9A8D-54CDE306EFF1 в папке data 130.96Кб 2019.07.25
			Файл 1316D384-5822-45C1-9C7D-94AD395C5D2B в папке data 2470.21Кб 2019.07.25
	Файл entodima обложка.aep в папке entodima 506.814Кб 2019.07.29
	Файл entodima.ru.sketch в папке entodima 2308.88Кб 2019.07.29
Файл nokia_9_pureview_camera_asia_26.jpg в папке Desktop 7159.279Кб 2019.07.30
Файл Mu3ePvOmkIg.jpg в папке Desktop 78.419Кб 2019.07.25
Файл laser blender.png в папке Desktop 642.366Кб 2019.07.25
Файл iSmartTable.sketch в папке Desktop 5501.356Кб 2019.07.25
Файл kSuDCHLhwkk.jpg в папке Desktop 126.424Кб 2019.07.25
Файл M6TcZeNDRqc.jpg в папке Desktop 115.69Кб 2019.07.25
Файл Screenshot 2019-07-25 at 18.22.02.png в папке Desktop 4634.176Кб 2019.07.29
Файл 15399003834570.png в папке Desktop 32.401Кб 2019.07.25
Файл My website clouds.sketch в папке Desktop 2920.161Кб 2019.07.25
Файл browser.sketch в папке Desktop 196.74Кб 2019.07.25
Файл Long-Curly-Fringe-High-Taper.jpg в папке Desktop 57.956Кб 2019.07.25
Файл 9d38d29aa57a1f4f94f23bf76947fcfc.jpg в папке Desktop 147.434Кб 2019.07.25
Файл Screenshot 2019-07-19 at 17.36.20.png в папке Desktop 819.926Кб 2019.07.29
Файл vlcsnap-2018-11-27-23h33m20s161.png в папке Desktop 1830.116Кб 2019.07.25
Файл tatu.jpg в папке Desktop 317.849Кб 2019.07.30
Файл IAD-RGZ-Ded.ded в папке Desktop 315.732Кб 2019.07.25
Файл timenote_app_redesign.png в папке Desktop 155.894Кб 2019.07.25
Файл Screenshot 2019-04-03 at 20.15.05.png в папке Desktop 1536.656Кб 2019.07.29
Файл Screenshot 2019-01-12 at 15.49.51.png в папке Desktop 51.023Кб 2019.07.25
Файл Screenshot 2019-01-17 at 13.04.12.png в папке Desktop 6124.902Кб 2019.07.29
Файл Screenshot 2019-02-27 at 23.28.48.png в папке Desktop 1322.417Кб 2019.07.25
';

        static::assertEquals($expected, scanDirectory('/Users/dima/Desktop'));
    }
}
