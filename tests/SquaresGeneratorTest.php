<?php
declare(strict_types=1);

namespace OOP;

include "../vendor/autoload.php";

use PHPUnit\Framework\TestCase;

/**
 * Class SquaresGeneratorTest
 * @package OOP
 */
class SquaresGeneratorTest extends TestCase
{

    public function testGenerate()
    {
        static::assertEquals([new Square(3), new Square(3)], SquaresGenerator::generate(3,2));
    }
}
