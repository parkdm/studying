<?php
declare(strict_types=1);

include "../vendor/autoload.php";

use OOP\Circle;
use PHPUnit\Framework\TestCase;

/**
 * Class CircleTest
 */
class CircleTest extends TestCase {
    /** @test **/
    function circleArea(){
        $circle = new Circle(10);
        static::assertEquals(314.15926535898, $circle->getArea());
    }

    /** @test **/
    function circleCircumference(){
        $circle = new Circle(10);
        static::assertEquals(62.831853071796, $circle->getCircumference());
    }
}
