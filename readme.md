### Задания для самопроверки
07 - [Массивы](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/07ArraysTasksImplementations.php)

### Итоговые задачи
07 - [Массивы](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/07ArraysTasksImplementations.php).

08 - [Ассоциативные массивы](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/08AssociativeArraysTasksImplementations.php). [Тесты](https://gitlab.rarus.ru/parkdm/studying/blob/master/tests/AssociativeTest.php).

09 - [Ввод/вывод](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/09IO.php). [Тесты](https://gitlab.rarus.ru/parkdm/studying/blob/master/tests/IOTest.php)

10 - [Функции](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/10FunctionsImplementations.php). [Тесты](https://gitlab.rarus.ru/parkdm/studying/blob/master/tests/FunctionsTest.php)

13 - ООП:

&nbsp;&nbsp;&nbsp;&nbsp; 13.1. [Circle](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/Circle.php). [Тест](https://gitlab.rarus.ru/parkdm/studying/blob/master/tests/CircleTest.php)

&nbsp;&nbsp;&nbsp;&nbsp; 13.2. [Random](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/Random.php). [Тест](https://gitlab.rarus.ru/parkdm/studying/blob/master/tests/RandomTest.php)

&nbsp;&nbsp;&nbsp;&nbsp; 13.3.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 13.3.1. [Url](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/Url.php). [Тест](https://gitlab.rarus.ru/parkdm/studying/blob/master/tests/UrlTest.php)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 13.3.1. [UrlInterface](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/UrlInterface.php). 

&nbsp;&nbsp;&nbsp;&nbsp; 13.4. [Square](https://gitlab.rarus.ru/parkdm/studying/blob/master/src/Square.php). [Тест](https://gitlab.rarus.ru/parkdm/studying/blob/master/tests/SquareTest.php)
