<?php
declare(strict_types=1);

namespace OOP
{
    require_once 'UrlInterface.php';

    use UrlInterface;

    /**
     * Class Url
     * @package OOP
     */
    class Url implements UrlInterface {
        public $url;

        /**
         * Url constructor.
         * @param $url
         */
        public function __construct($url){
            $this->url = parse_url($url);

            $this->url['query'] = explode('&', $this->url['query']);
            $this->url['query'] = array_reduce($this->url['query'], function ($accumulator,$queryRow){
                $queryRow = explode('=', $queryRow);
                $accumulator[$queryRow[0]] = $queryRow[1];
                return $accumulator;
            });
        }

        /**
         * @return mixed
         */
        public function getScheme(){
            return $this->url['scheme'];
        }

        /**
         * @return mixed
         */
        public function getHost(){
            return $this->url['host'];
        }

        /**
         * @return mixed
         */
        public function getQueryParams(){
            return $this->url['query'];
        }

        /**
         * @param $key
         * @param null $default
         * @return mixed
         */
        public function getQueryParam($key, $default=null){
            if (isset($this->url['query'][$key])){
                return $this->url['query'][$key];
            }
            return $default;
        }
    }
}