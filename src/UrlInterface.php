<?php
declare(strict_types=1);

/**
 * Interface UrlInterface
 */
interface UrlInterface{
    /**
     * UrlInterface constructor.
     * @param $url
     */
    public function __construct($url);
    public function getScheme();
    public function getQueryParams();

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getQueryParam($key, $default=null);
}
