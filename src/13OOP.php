<?php
declare(strict_types=1);

require 'vendor/autoload.php';

// 13.1 Круг
// Реализуйте класс Circle для описания кругов. У круга есть только одно свойство - его радиус.
// Реализуйте методы getArea и getCircumference, которые возвращают площадь и периметр круга соответственно.
// Площадь круга: π*r^2
// Длина окружности: 2*π*r
//include 'Circle.php';
//use OOP\Circle;
//$circle = new Circle(10);
//echo 'Площадь: '.$circle->getArea().PHP_EOL.'Длинна окружности: '.$circle->getCircumference();


// 13.2 Генератор случайных чисел
// Реализуйте генератор рандомных чисел, представленный классом Random. Интерфейс объекта включает в себя три функции:
//
// Конструктор. Принимает на вход seed, начальное число генератора псевдослучайных чисел
// getNext — метод, возвращающий новое случайное число
// reset — метод, сбрасывающий генератор на начальное значение
//include 'Random.php';
//use OOP\Random;
//$random = new Random(100);
//$result1 = $random->getNext();
//$result2 = $random->getNext();
//print_r($result1 != $result2); // => true
//
//$random->reset();
//
//$result21 = $random->getNext();
//$result22 = $random->getNext();
//
//print_r($result1 == $result21); // => true
//print_r($result2 == $result22); // => true


// 13.3 Url
// Реализуйте класс Url который описывает переданный в конструктор HTTP адрес и позволяет извлекать из него части.
// То что нужно реализовать описано в интерфейсе UrlInterface
// Для разбора адреса воспользуйтесь функцией parse_url
// Query Params придется разобрать самостоятельно
//include 'Url.php';
//use OOP\Url;
//$url = new Url('http://yandex.ru?key=value&key2=value2');
//print_r($url);
//print_r($url->getScheme()); // http
//$url->getHost(); // yandex.ru
//$url->getQueryParams();
//// [
////     'key' => 'value',
////     'key2' => 'value2'
//// ];
//$url->getQueryParam('key'); // value
//// второй параметр - значение по умолчанию
//$url->getQueryParam('key2', 'lala'); // value2
//$url->getQueryParam('new', 'ehu'); // ehu


//13.4 Генератор квадратов
// Реализуйте класс Square для описания квадратов. У квадрата есть только одно свойство — сторона.
// Реализуйте метод getSide, возвращающий значение стороны.
//include 'Square.php';
//include 'SquaresGenerator.php';
//use OOP\Square;
//use OOP\SquaresGenerator;
//
//$square = new Square(10);
//$square->getSide(); // 10
//src/SquaresGenerator.php
// Реализуйте класс SquaresGenerator со статическим методом generate, принимающим два параметра:
// сторону и количество экземпляров квадрата, которые нужно создать. Функция должна вернуть массив из квадратов.
//
//print_r($squares = SquaresGenerator::generate(3, 2));
// [new Square(3), new Square(3)];


echo PHP_EOL;
