<?php
declare(strict_types=1);


namespace OOP;


/**
 * Class SquaresGenerator
 * @package OOP
 */
class SquaresGenerator
{
    /**
     * @param $side
     * @param $count
     * @return array
     */
    static function generate($side, $count){
        $output = [];
        for ($current=0; $current < $count; $current++) $output[] = new Square($side);
        return $output;
    }
}