<?php

namespace App\Arrays{
    function get($array, $n, $default=null){
        if(isset($array[$n])){
            return $array[$n];
        } else {
            return $default;
        }
    }

    function addPrefix($array, $prefix){
        $new_array = [];
        foreach ($array as $element){
            if(gettype($element)=="string"){
                $new_array[] = $prefix." ".$element;
            }
        }
        return $new_array;
    }

    function swap($array, $id){
        if($id>0 && sizeof($array)>$id+1){
            $new_array = $array;

            $new_array[$id-1] = $array[$id+1];
            $new_array[$id+1] = $array[$id-1]; 
            
            return $new_array;
        }
        return $array;
    }

    function calculateAverage($array){
        $size = sizeof($array);
        if($size == 0){
            return null;
        }
        $summ=0;
        foreach ($array as $element){
            $summ+=$element;
        }
        $summ /= $size;
        return $summ;
    }

    function findIndex($array, $value){
        $returned = -1;
        if(!empty($array)) {
            for($i=0; $i<sizeof($array); $i++){
                if ($array[$i]==$value){
                    $returned = $i;
                    break;
                }
            }
        }
        return $returned;
    }

    function getSameParity($array){
        if(!empty($array)){
            $new_array=[];
            $mod = $array[0] % 2;
            foreach ($array as $element){
                if(($element % 2) == $mod){
                    $new_array[] = $element;
                }
            }
            return $new_array;
        }
        
    }

    function getIndexOfWarmestDay($array){
        $temperature=0;
        $index=0;
        foreach ($array as $key => $element) {
            $max_day = max($element);
            if($max_day>$temperature){
                $temperature = $max_day;
                $index = $key;
            }
        }
        return $index;
    }
    
    function buildDefinitionList($array){
        $parts = [];
        foreach ($array as $row){
            $parts[] = "<dt>{$row[0]}</dt><dd>{$row[1]}</dd>";
        }
        $innerValue = implode($parts);
        return "<dl>{$innerValue}</dl>";
    }

    function getSameCount($array1, $array2){
        $count = [];
        foreach($array1 as $element1){
            foreach($array2 as $element2){
                if($element1==$element2 && !in_array($element1, $count)){
                    $count[] = $element1;
                }
            }
        }
        return sizeof($count);
    }
    
    function getIntersectionOfSortedArray($array1, $array2){
        $intersections = [];
        if (!empty($array1) && !empty($array2)){
            foreach ($array1 as $element1){
                foreach ($array2 as $element2){
                    if ($element1 == $element2 and !in_array($element1, $intersections)){
                        $intersections[] = $element1;
                        break;
                    }
                }
            }
        }
        return $intersections;
    }
}

namespace App\Strings{
    function makeCensored($string, $words){
        $string_array = explode(' ', $string);
        $new_array = [];
        foreach ($string_array as $element) {
            foreach ($words as $word){
                $element==$word ? $new_array[] = '$#%!': $new_array[] = $element;
            }
        }
        return implode(' ', $new_array);
    }

    function countUniqChars($string){
        $symbols = [];
        for($i=0; $i<strlen($string); $i++){
            if(!in_array($string[$i], $symbols)){
                $symbols[] = $string[$i];
            }
        }
        return sizeof($symbols);
    }

    function checkIfBalanced($expression){
        $stack = [];
        $startSymbols = ['('];
        $pairs = ['()'];
        
        for ($i = 0; $i < strlen($expression); $i++) {
            $curr = $expression[$i];
            if (in_array($curr, $startSymbols)) {
                array_push($stack, $expression[$i]);
            } else {
                $prev = array_pop($stack);
                $pair = "{$prev}{$curr}";
                if (!in_array($pair, $pairs)) {
                    return false;
                }
            }
        }
        return sizeof($stack) == 0;
    }
         
}
