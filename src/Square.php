<?php
declare(strict_types=1);


namespace OOP;


/**
 * Class Square
 * @package OOP
 */
class Square
{
    protected $side;
    /**
     * Square constructor.
     * @param $side
     */
    public function __construct($side)
    {
        $this->side = $side;
    }

    public function getSide()
    {
        return $this->side;
    }
}