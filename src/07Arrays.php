<?php
include '07ArraysFunctions.php';

//7-2
// use function App\Arrays\get;
// $cities = ['moscow', 'london', 'berlin', 'porto'];
// echo get($cities, 1); // => london
// echo get($cities, 4); // => null
// echo get($cities, 10, 'paris'); // => paris


//7-3
// use function App\Arrays\addPrefix;
// $names = ['john', 'smith', 'karl'];
// $newNames = addPrefix($names, 'Mr');
// print_r($newNames);


//7-4
// use function App\Arrays\swap;
// $names = ['john', 'smith', 'karl'];
// $result = swap($names, 1);
// print_r($result); // => ['karl', 'smith', 'john']
// echo "\n";
// $result = swap($names, 2);
// print_r($result); // => ['john', 'smith', 'karl']
// echo "\n";
// $result = swap($names, 0);
// print_r($result); // => ['john', 'smith', 'karl']


//7-6
// use function App\Arrays\calculateAverage;
// $temperatures = [37.5, 34, 39.3, 40, 38.7, 41.5];
// print_r(calculateAverage($temperatures)); // => 38.5
// $temperatures = [];
// print_r(calculateAverage($temperatures)); // => null


//7-8
// use function App\Arrays\findIndex;
// $temperatures = [37.5, 34, 39.3, 40, 38.7, 41.5, 40];
// print_r(findIndex($temperatures, 34)); // => 1
// print_r(findIndex($temperatures, 40)); // => 3
// print_r(findIndex($temperatures, 3));  // => -1


//7-9
// use function App\Arrays\getSameParity;
// print_r(getSameParity([]));        // => []
// print_r(getSameParity([1, 2, 3])); // => [1, 3]
// print_r(getSameParity([1, 2, 8])); // => [1]
// print_r(getSameParity([2, 2, 8])); // => [2, 2, 8]


//7-10
// use function App\Arrays\getIndexOfWarmestDay;
// $data = [
//     [-5, 7, 1],
//     [3, 2, 3],
//     [-1, -1, 10],
// ];
// print_r(getIndexOfWarmestDay($data)); // 2


//7-11
// use function App\Arrays\buildDefinitionList;
// $definitions = [
//     ['Блямба', 'Выпуклость, утолщения на поверхности чего-либо'],
//     ['Бобр', 'Животное из отряда грызунов'],
// ];

// print_r(buildDefinitionList($definitions)); // => '<dl><dt>Блямба</dt><dd>Выпуклость...</dd><dt>Бобр</dt><dd>Живтоное...</dd></dl>';


//7-12
// use function App\Strings\makeCensored;
// $sentence = 'When you play the game of thrones, you win or you die';
// print_r(makeCensored($sentence, ['die', 'play']));
// // => When you $#%! the game of thrones, you win or you $#%!

// $sentence2 = 'chicken chicken? chicken! chicken';
// print_r(makeCensored($sentence2, ['?', 'chicken']));
// // => '$#%! chicken? chicken! $#%!';


//7-13
// use function App\Arrays\getSameCount;
// print_r(getSameCount([], [])); // => 0
// print_r(getSameCount([1, 10, 3], [10, 100, 35, 1])); // => 2
// print_r(getSameCount([1, 3, 2, 2], [3, 1, 1, 2])); // => 3


//7-14
// use function App\Strings\countUniqChars;
// $text1 = 'yyab';
// print_r(countUniqChars($text1)); // => 3
// $text2 = 'You know nothing Jon Snow';
// print_r(countUniqChars($text2)); // => 13
// $text3 = '';
// print_r(countUniqChars($text3)); // => 0


//7-17
// use function App\Strings\checkIfBalanced;
// print_r(checkIfBalanced('(5 + 6) * (7 + 8)/(4 + 3)')); // => true
// print_r(checkIfBalanced('(4 + 3))')); // => false


//7-18
//use function App\Arrays\getIntersectionOfSortedArray;
//print_r(getIntersectionOfSortedArray([10, 11, 24], [10, 13, 14, 18, 24, 30])); // => [10, 24]


echo "\n";