<?php
declare(strict_types=1);


namespace OOP {
    /**
     * Class Circle
     * @package OOP
     */
    class Circle {
        private $radius;

        private $square;
        private $circumference;

        /**
         * Circle constructor.
         * @param $radius
         */
        function __construct($radius) {
            $this->radius = $radius;
            $this->calculateProperties();
        }

        private function calculateProperties(){
            $pi = pi();
            $this->square = $pi * $this->radius ** 2;
            $this->circumference = 2 * $pi * $this->radius;
        }

        /**
         * @return mixed
         */
        public function getArea(){
            return $this->square;
        }

        /**
         * @return mixed
         */
        public function getCircumference(){
            return $this->circumference;
        }
    }
}

