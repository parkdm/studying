<?php

include '07ArraysTasksImplementations.php';

//7-1
//use function App\Arrays\Tasks\getChunked;
//print_r(getChunked(['a', 'b', 'c', 'd'], 2)); // → [['a', 'b'], ['c', 'd']]
//print_r(getChunked(['a', 'b', 'c', 'd'], 3)); // → [['a', 'b', 'c'], ['d']]


//7-2
//use function App\Arrays\Tasks\getMirrorMatrix;
//print_r(getMirrorMatrix([
//    [11, 12, 13, 14],
//    [21, 22, 23, 24],
//    [31, 32, 33, 34],
//    [41, 42, 43, 44],
//]));
// → [
//     [11, 12, 12, 11],
//     [21, 22, 22, 21],
//     [31, 32, 32, 31],
//     [41, 42, 42, 41],
//   ]


//7-3
//use function App\Strings\Tasks\compareVersion;
//print_r(compareVersion("0.1", "0.2")); // → -1
//print_r(compareVersion("0.2", "0.1")); // → 1
//print_r(compareVersion("4.2", "4.2")); // → 0


//7-4
//use function App\Arrays\Tasks\summaryRanges;
//print_r(summaryRanges([1, 2, 3])); // → ["1->3"]
//print_r(summaryRanges([110, 111, 112, 111, -5, -4, -2, -3, -4, -5])); // → ['110->112', '-5->-4']


//7-5
//use function App\Strings\Tasks\hammingWeight;
//print_r(hammingWeight(0)); // → 0
//print_r(hammingWeight(4)); // → 1
//print_r(hammingWeight(101)); // → 4


//7-6
//use function App\Strings\Tasks\longestLength;
//print_r(longestLength('abcdeef')); // → 5
//echo "\n";
//print_r(longestLength('jabjcdel')); // → 7


//7-7
//use function App\Strings\Tasks\lengthOfLastWord;
//print_r(lengthOfLastWord('')); // 0
//echo "\n";
//print_r(lengthOfLastWord('man in BlacK')); // 5
//echo "\n";
//print_r(lengthOfLastWord('hello, world!  ')); // 6


//7-8
//use function App\Arrays\Tasks\generatePascalsTriangle;
//print_r(generatePascalsTriangle(1)); // → [1, 1]
//echo "\n";
//print_r(generatePascalsTriangle(4)); // → [1, 4, 6, 4, 1]
//echo "\n";


//7-9
//use function App\Arrays\Tasks\calcInPolishNotation;
//print_r(calcInPolishNotation([1, 2, '+', 4, '*', 3, '+'])); // → 15
//echo "\n";
//print_r(calcInPolishNotation([7, 2, 3, '*', '-'])); // → 1
//echo "\n";


echo "\n";
