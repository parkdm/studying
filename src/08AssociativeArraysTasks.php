<?php
declare(strict_types=1);

include '08AssociativeArraysTasksImplementations.php';


//8-1
//use function App\AssocArrays\fromParis;
//print_r(fromParis([['fred', 30], ['barney', 40]])); // → ['fred' => 30, 'barney' => 40]


//8-2
// Имена параметров в выходной строке должны располагаться в алфавитном порядке (то есть их нужно отсортировать).
//use function App\AssocArrays\buildQueryString;
//print_r(buildQueryString(['per' => 10, 'page' => 1])); // → page=1&per=10


//8-3
//use function App\AssocArrays\findWhere;
//print_r(findWhere(
//    [
//        [
//            'title' => 'Book of Fooos',
//            'author' => 'FooBar',
//            'year' => 1111
//        ],
//        [
//            'title' => 'Cymbeline',
//            'author' => 'Shakespeare',
//            'year' => 1611
//        ],
//        [
//            'title' => 'The Tempest',
//            'author' => 'Shakespeare',
//            'year' => 1611
//        ],
//        [
//            'title' => 'Book of Foos Barrrs',
//            'author' => 'FooBar',
//            'year' => 2222
//        ],
//        [
//            'title' => 'Still foooing',
//            'author' => 'FooBar',
//            'year' => 3333
//        ],
//        [
//            'title' => 'Happy Foo',
//            'author' => 'FooBar',
//            'year' => 4444
//        ],
//    ],
//    [
//        'author' => 'Shakespeare',
//        'year' => 1611
//    ]
//)); // => ['title' => 'Cymbeline', 'author' => 'Shakespeare', 'year' => 1611]


//8-4
//use function App\AssocArrays\toRna;
//print_r(toRna('ACGTGGTCTTAA')); // → 'UGCACCAGAAUU'


echo "\n";