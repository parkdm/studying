<?php
declare(strict_types=1);

namespace App\AssocArrays {

    /**
     * @param $array
     * @return array
     */
    function fromParis($array){
        $newArray = [];
        foreach ($array as $element) {
            $newArray[$element[0]] = $element[1];
        }
        return $newArray;
    }

    /**
     * @param $array
     * @return string
     */
    function buildQueryString($array) {
        ksort($array);
        $newArray = [];
        foreach ($array as $key => $element){
            $newArray[] = $key.'='.$element;
        }
        return implode('&', $newArray);
    }

    /**
     * @param $books
     * @param $params
     * @return mixed
     */
    function findWhere($books, $params) {
        $finded = null;

        foreach ($books as $book){
            $finded = $book;

            foreach ($book as $bookParamKey => $bookParam){
                foreach ($params as $paramKey => $param){
                    if ($bookParamKey==$paramKey){
                        if ($bookParam != $param) {
                            $finded = null;
                            break 2;
                        } elseif (end($params) == $param){
                            break 3;
                        }
                    }
                }
            }
        }

        return $finded;
    }

    /**
     * @param $string
     * @return string
     */
    function toRna($string) {
        $toRnaDict = [
            'G'=>'C',
            'C'=>'G',
            'T'=>'A',
            'A'=>'U',
        ];
        $dnaArr = str_split($string);

        foreach ($dnaArr as &$dnaItem){
            if (in_array($dnaItem, array_keys($toRnaDict))){
                $dnaItem = $toRnaDict[$dnaItem];
            } else {
                return null;
            }
        }

        return implode('', $dnaArr);
    }
}
