<?php
declare(strict_types=1);


/**
 * Class Random
 */
namespace OOP{

    /**
     * Class Random
     * @package OOP
     */
    class Random {
        public $value;
        private $seed;

        /**
         * Random constructor.
         * @param $seed
         */
        public function __construct($seed){
            $this->value = $this->seed = $seed;
        }

        /**
         * @return int
         */
        public function getNext(){
            $this->value = rand();
            return $this->value;
        }

        public function reset(){
            $this->value=$this->seed;
        }
    }
}