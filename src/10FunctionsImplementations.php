<?php

declare(strict_types=1);

namespace App\Functions {

    /**
     * @param $array
     * @param $value
     * @return int
     */
    function findIndexOfNearest($array, $value){
        if(empty($array)) return null;

        # поиск element, ближайшего к value (не нашел как передать id в array_reduce)
        $closeValue = array_reduce($array, function ($accumulator, $element) use ($value){
            return abs($element-$value) < abs($accumulator-$value) ? $element : $accumulator;
        }, $array[0]);

        # возврат индекса ближайшего value
        return array_search($closeValue, $array);
    }

    /**
     * @param $array
     * @return mixed
     */
    function getSameParity($array){
        if (empty($array)) return [];

        $firstNumberParity = abs($array[0]%2);
        return array_reduce($array, function ($accumulator, $element) use ($firstNumberParity){
            if (abs($element%2) === $firstNumberParity) $accumulator[] = $element;
            return $accumulator;
        });
    }

    /**
     * @param $inputString
     * @return string
     */
    function decode($inputString) {
        $inputString = preg_replace('/[|][_,¯]/mu', '1', $inputString);
        return preg_replace('/[_,¯]/mu', '0', $inputString);
    }

    /**
     * @param $array
     * @return array
     */
    function enlargeArrayImage($array, $multiplier=2):array {
        return array_reduce($array, function ($accumulator, $string) use ($multiplier) {
            $string = array_reduce($string, function ($accumulator2, $column) use ($multiplier) {
                for ($temporary=0; $temporary<$multiplier; $temporary++) $accumulator2[] = $column;
                return $accumulator2;
            });

            for ($temporary=0; $temporary<$multiplier; $temporary++) $accumulator[] = $string;
            return $accumulator;
        });
    }
}
