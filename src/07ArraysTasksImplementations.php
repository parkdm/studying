<?php
namespace App\Arrays\Tasks{
    function getChunked($array, $n){
        $new_array=[];
        $temp_array=[];
        foreach ($array as $element){
            $temp_array[] = $element;
            if (sizeof($temp_array)==$n){
                $new_array[] = $temp_array;
                $temp_array = [];
            }
        }
        return $new_array;
    }

    function getMirrorMatrix($array){
        $new_array = $array;
        $size = sizeof($array);
        for ($i=0; $i<$size; $i++){
            for ($j=$size/2; $j<$size; $j++){
                $new_array[$i][$j] = $new_array[$i][$size-1-$j];
            }
        }
        return $new_array;
    }

    /**
     * @param $array
     * @return array
     */
    function summaryRanges($array){
        $sequences = [];
        $sequenceTemp = [];

        $sequence_temp_size = 0;
        foreach ($array as $element){
            $sequence_temp_size = sizeof($sequenceTemp);
            if ($sequence_temp_size>0){
                if (end($sequenceTemp)+1 != $element){
                    if ($sequence_temp_size>1) $sequences[] = $sequenceTemp;
                    $sequenceTemp = [];
                } else {
                    $sequenceTemp[] = $element;
                }
            } else {
                $sequenceTemp[] = $element;
            }
        }
        if ($sequence_temp_size>1) $sequences[] = $sequenceTemp;

        $returned_sequences = [];
        foreach ($sequences as $sequence){
            $returned_sequences[] = $sequence[0].'->'.end($sequence);
        }

        return $returned_sequences;
    }

    /**
     * @param $n
     * @return array
     */
    function generatePascalsTriangle($n){
        $n++;
        $triangle = [];
        for ($i=0; $i<$n; $i++){
            $temp = [1];
            for ($j=1; $j<$n; $j++) $temp[] = 0;
            $triangle [] = $temp;
        }

        for ($i=1; $i<$n; $i++){
            for ($j=1; $j<$n; $j++){
                $triangle[$i][$j] = $triangle[$i-1][$j]+$triangle[$i-1][$j-1];
            }
        }
        return end($triangle);
    }

    /**
     * @param $array
     * @return array
     */
    function calcInPolishNotation($array){
        $operations = ['+', '-', '*', '/'];
        $stack = [];

        foreach ($array as $element){
            if (in_array($element, $operations)){
                $previous1 = (int)array_pop($stack);
                $previous2 = (int)array_pop($stack);

                if ($element == '+'){
                    $result = $previous2 + $previous1;
                }
                if ($element == '-'){
                    $result = $previous2 - $previous1;
                }
                if ($element == '/'){
                    $result = $previous2 / $previous1;
                }
                if ($element == '*'){
                    $result = $previous2 * $previous1;
                }
                array_push($stack, $result);
            } else {
                array_push($stack, $element);
            }
        }

        return $stack;
    }
}

namespace App\Strings\Tasks{

    /**
     * @param $version1
     * @param $version2
     * @return int|mixed
     */
    function compareVersion($version1, $version2){
        $version1_array = explode('.', $version1);
        $version2_array = explode('.', $version2);

        $result_array = [];
        for ($i=0; $i<sizeof($version1_array); $i++){
            if ($version1_array[$i]>$version2_array[$i]){
                $result_array[$i] = 1;
            } else if ($version1_array[$i]<$version2_array[$i]){
                $result_array[$i] = -1;
            } else {
                $result_array[$i] = 0;
            }
        }

        $result = 0;

        for ($i=0;$i<sizeof($result_array); $i++){
            if ($result_array[$i]!=0) $result = $result_array[$i];
        }

        return $result;
    }

    function hammingWeight($value){
        $value_bin = decbin($value);
        return substr_count ($value_bin, 1);
    }

    function longestLength($string){
        $string_length = strlen($string);
        $temp_string = '';
        $longest_string_length = 0;

        for ($i=0; $i<$string_length; $i++){
            for ($j=$i; $j<$string_length-1; $j++){
                $temp_string_length = strlen($temp_string);
                if ($temp_string_length>0){
                    if (strpos($temp_string, $string[$j]) == null){   //если символ не входит во временный массив
                        $temp_string .= $string[$j];
                    } else {
                        $temp_string = '';
                    }
                } else {
                    $temp_string .= $string[$j];
                }

                //если длинна временного больше, то изменить максимально длинную последовательность
                $temp_string_length = strlen($temp_string);
                if ($temp_string_length>$longest_string_length){
                    $longest_string_length = $temp_string_length;
                }
            }
        }

        return $longest_string_length;
    }

    /**
     * @param $string
     * @return int
     */
    function lengthOfLastWord($string){
        $new_string = trim($string);
        $i = strlen($new_string)-1;
        $size = 0;
        while ($i >= 0 and $new_string[$i] != ' '){
            $size++;
            $i--;
        }
        return $size;
    }
}