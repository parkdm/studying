<?php
declare(strict_types=1);

/**
 * @param $path
 * @return mixed
 */
function getParentDirectory($path){
    $temporary = explode(DIRECTORY_SEPARATOR, $path);
    return end($temporary);
}

/**
 * @param $directory
 * @param string $spaces
 * @return string
 */
function scanDirectory($directory, $spaces=''){
    # Получаем объект просмотра файловой системы
    $iterator = new DirectoryIterator($directory);

    $listing = '';

    foreach($iterator as $element){ # Обходим элементы, содержажиеся в папке
        # Если элемент - "." или ".."
        if($element->isDot()) continue; # Игнорируем его, не внося в общий список

        # Если элемент - файл
        if ($element->isFile()) {
            $listing .= $spaces . 'Файл '. $element->getFilename() .' в папке '.getParentDirectory($element->getPath()).
                ' '.$element->getSize()/1000 .'Кб '.date ("Y.m.d", fileatime($element->getRealPath())).
                PHP_EOL; # Добавляем элемент в список
        }

        # Если элемент - папка и она пуста
        if($element->isDir() && glob($element->getRealPath().DIRECTORY_SEPARATOR.'*')) {
            $listing .= $spaces . 'Папка '. $element->getFilename().' - '.
                date ("Y.m.d", fileatime($element->getRealPath())).PHP_EOL;
            $listing .= scanDirectory($element->getRealPath(), "{$spaces}\t"); # Добавляем элемент в список
        }
    }

    return $listing;
}

$input = '/Users/dima/Desktop';
//$input = $argv[1];
if ($input[-1]==DIRECTORY_SEPARATOR) $input = substr($input,0,-2);

if (file_exists($input) && is_readable($input)){
    $listing = scanDirectory($input);

    $filename = __DIR__.DIRECTORY_SEPARATOR.getParentDirectory($input).' from '.date('Y-m-d H:i:s').'.txt';
    echo $filename.PHP_EOL;
    file_put_contents($filename, $listing) or die ("Unable to write file!");
}


